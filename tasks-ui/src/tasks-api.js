const tasksBaseUrl = '/api/task';

const tasks = {
  create: async () => {
    const response = await fetch(tasksBaseUrl);
    if (!response.ok) {
      throw Error(`${response.status}: ${response.text()}`);
    }
    return response.text();
  },
  getStatus: async (id) => {
    const response = await fetch(`${tasksBaseUrl}/${id}`);
    if (!response.ok) {
      throw Error(`${response.status}: ${response.text()}`);
    }
    return response.json();
  },
  waitUntilFinished: async (id) => {
    const response = await fetch(`${tasksBaseUrl}/${id}/finished`);
    if (response.status === 408) {
      return response.json();
    }
    if (!response.ok) {
      throw Error(`${response.status}: ${response.text()}`);
    }
    return response.json();
  },
  statuses: {
    CREATED: 'created',
    RUNNING: 'running',
    FINISHED: 'finished',
    TIMEOUT: 'timeout',
  },
};

export default tasks;