import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Badge from 'react-bootstrap/Badge';
import Card from 'react-bootstrap/Card';

import tasks from './tasks-api';

const taskStatusToBootstrapColor = {
  [tasks.statuses.CREATED]: 'primary',
  [tasks.statuses.RUNNING]: 'warning',
  [tasks.statuses.FINISHED]: 'success',
  [tasks.statuses.TIMEOUT]: 'danger',
};

function App() {
  const [taskList, setTaskList] = useState([]);

  async function createTask() {
    const taskId = await tasks.create();
    const task = {
      taskId,
      status: tasks.statuses.CREATED,
      timestamp: new Date().toISOString(),
    };
    setTaskList([task, ...taskList]);
  }
  return (
    <Container>
      <Row>
        <h2>Task manager</h2>
      </Row>
      <Row>
        <Button onClick={createTask}>Create task</Button>
      </Row>
      { taskList.map(task => (<TaskOperations task={task} key={task.taskId}/>)) }
    </Container>
  );
}

function TaskOperations({ task }) {
  const [status, setStatus] = useState(task.status);
  const [timestamp, setTimestamp] = useState(task.timestamp);
  const [isWaiting, setWaiting] = useState(false);

  async function getStatus() {
    const taskInfo = await tasks.getStatus(task.taskId);
    setStatus(taskInfo.status);
    setTimestamp(taskInfo.timestamp);
  }

  async function waitUntilFinished() {
    setWaiting(true);
    const taskInfo = await tasks.waitUntilFinished(task.taskId);
    if (taskInfo.error) {
      setStatus(tasks.statuses.TIMEOUT);
    } else {
      setStatus(taskInfo.status);
      setTimestamp(taskInfo.timestamp);
    }
    setWaiting(false);
  }

  return (
    <Row>
      <Card style={{ width: '100%' }}>
        <Card.Body>
          <Card.Title>Task {task.taskId}</Card.Title>
          <Card.Text>
            <Badge pill variant={taskStatusToBootstrapColor[status]}>{status}</Badge> Timestamp: {timestamp}
          </Card.Text>
          <ButtonToolbar>
            <Button onClick={getStatus}>Get status</Button>
            <Button
              disabled={isWaiting}
              onClick={!isWaiting ? waitUntilFinished : null}
            >
              {isWaiting ? 'Waiting...' : 'Wait until finished'}
            </Button>
          </ButtonToolbar>
        </Card.Body>
      </Card>
    </Row>
  );
}

export default App;
