const { env } = process;

module.exports = {
  taskCheckTimeoutMs: parseInt(env.TASK_TIMEOUT_MS, 10) || 300000,
  taskCheckIntervalMs: parseInt(env.TASK_CHECK_INTERVAL_MS, 10) || 5000,
  taskRunTimeMs: parseInt(env.TASK_RUN_TIME_MS, 10) || 120000,
  serverInitIntervalMs: parseInt(env.SERVER_INIT_INTERVAL_MS, 10) || 5000,
  serverPort: parseInt(env.SERVER_PORT, 10) || 8000,
  mongoConnectionString: env.MONGO_CONNECTION_STRING || 'mongodb://mongo:27017/test',
};
