const uuid = require('uuid/v4');
const { statuses } = require('./consts');

function timer(timeoutMs) {
  return new Promise(resolve => setTimeout(() => resolve(null), timeoutMs));
}

function createController({ storage, logger, config }) {
  async function startTask(taskId) {
    logger.log('starting task', taskId);
    await storage.update(taskId, { status: statuses.RUNNING });
    setTimeout(async () => {
      logger.log(`finished task ${taskId}, updating db`);
      await storage.update(taskId, { status: statuses.FINISHED });
      logger.log('updated db for task', taskId);
    }, config.taskRunTimeMs);
  }

  async function createTask(req, res) {
    const taskId = uuid();
    try {
      const payload = { status: statuses.CREATED, taskId };
      logger.log('creating task', payload);
      await storage.create(payload);
      logger.log('created task', taskId);
      res.status(202).send(taskId);
    } catch (err) {
      logger.error('error creating task', taskId, err.message || err.toString());
      res.status(500).json({ error: 'something went wrong' });
    } finally {
      startTask(taskId);
    }
  }

  async function getTaskStatus(req, res) {
    const taskId = req.params.id;
    try {
      const task = await storage.getById(taskId);
      if (!task) {
        logger.log('task not found', taskId);
        return res.status(404).json({ error: 'not found' });
      }
      const result = {
        status: task.status,
        timestamp: task.updatedAt,
      };
      logger.log('got status for task', taskId, result);
      return res.status(200).json(result);
    } catch (err) {
      logger.error('error getting task status', err.message || err.toString());
      return res.status(500).json({ error: 'something went wrong' });
    }
  }

  async function waitForTask(taskId) {
    logger.log(`waiting for ${config.taskCheckIntervalMs} ms...`);
    await timer(config.taskCheckIntervalMs);
    const task = await storage.getById(taskId);
    if (task.status === statuses.FINISHED) {
      logger.log(`task ${taskId} is finished`);
      return task;
    }
    logger.log(`task ${taskId} is still not finished`);
    return waitForTask(taskId);
  }

  async function getTaskStatusWhenFinished(req, res) {
    const taskId = req.params.id;
    try {
      let task = await storage.getById(taskId);
      if (!task) {
        logger.log('task not found', taskId);
        return res.status(404).json({ error: 'not found' });
      }
      if (task.status !== statuses.FINISHED) {
        task = await Promise.race([waitForTask(taskId), timer(config.taskCheckTimeoutMs)]);
      }
      if (!task) {
        logger.error('timeout while waiting for task', taskId);
        return res.status(408).json({ error: 'timeout' });
      }
      const result = {
        status: task.status,
        timestamp: task.updatedAt,
      };
      return res.status(200).json(result);
    } catch (err) {
      logger.error('error waiting for task to be finished', taskId, err.message || err.toString());
      return res.status(500).json({ error: 'something went wrong' });
    }
  }

  return {
    createTask,
    getTaskStatus,
    getTaskStatusWhenFinished,
  };
}

module.exports = createController;
