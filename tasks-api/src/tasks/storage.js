function createTaskStorage({ mongoose }) {
  const schema = new mongoose.Schema(
    {
      status: { type: String, required: true },
      taskId: { type: String, required: true, unique: true },
    },
    { timestamps: true },
  );
  const Task = mongoose.model('tasks', schema);

  async function create(payload) {
    const task = new Task(payload);
    return task.save();
  }

  function update(taskId, payload) {
    return Task.findOneAndUpdate({ taskId }, payload, { new: true })
      .lean()
      .exec();
  }

  function getById(taskId) {
    return Task.findOne({ taskId })
      .lean()
      .exec();
  }

  return {
    create,
    update,
    getById,
  };
}

module.exports = createTaskStorage;
