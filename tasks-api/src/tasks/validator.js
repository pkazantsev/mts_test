const { celebrate, Joi } = require('celebrate');

const getById = {
  params: {
    id: Joi.string()
      .guid({
        version: ['uuidv4'],
      })
      .required(),
  },
};

module.exports = {
  getById: celebrate(getById),
};
