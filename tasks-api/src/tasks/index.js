const createStorage = require('./storage');
const createController = require('./controller');
const validator = require('./validator');

function createTasks({ express, mongoose, logger, config }) {
  const storage = createStorage({ mongoose });
  const controller = createController({ storage, logger, config });
  const routes = express.Router();

  routes.get('/', controller.createTask);
  routes.get('/:id', validator.getById, controller.getTaskStatus);
  routes.get('/:id/finished', validator.getById, controller.getTaskStatusWhenFinished);

  return { routes };
}

module.exports = createTasks;
