module.exports = {
  statuses: {
    CREATED: 'created',
    RUNNING: 'running',
    FINISHED: 'finished',
  },
};
