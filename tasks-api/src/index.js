const express = require('express');
const mongoose = require('mongoose');
const { errors } = require('celebrate');
const config = require('./config');
const createTasks = require('./tasks');
const createLogger = require('./logger');

const logger = createLogger();
const tasks = createTasks({ express, mongoose, logger, config });

async function startApp() {
  try {
    await mongoose.connect(config.mongoConnectionString, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    const app = express();
    app.use('/task', tasks.routes);
    app.use(errors());
    app.listen(config.serverPort, () => logger.log(`server is listening on port ${config.serverPort}`));
  } catch (err) {
    logger.error(err.message || err.toString());
    setTimeout(startApp, config.serverInitIntervalMs);
  }
}

startApp();
